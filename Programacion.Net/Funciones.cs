using System;
 
public class EstructurasDeControl
 {
	public static void Main () 
	{
	// Para usar la funcion ponemos el nombre y entre parentesis los argumentos (param)
	double y; 
	string valorUsuario;
	double valUsu;
	Console.WriteLine("introduzca valor x: ");
	valorUsuario = Console.ReadLine();
	valUsu = Double.Parse(valorUsuario);
	y = FuncionLineal2x3(valUsu );
	//Console.WriteLine("Resultado 2*X + 3: " + y);
	Console.WriteLine("Resultado X^2 + 1: " + y);
	
	}	
	// Forma de una funcion estatica
	// <Modificador acceso> static <tipo de dato resultado> <Nombre de la funcion> (<Tipo>Parametro1<Tipo2>Parametro2...)
	// y luego entre llaves el cuerpo de la funcion
	// con return podemos devolver un valor
	private static double FuncionLineal2x3(double x)
	{
		return x * x + 1;
		
		
	}

 }	