using System;

// tutorialesprogramacionya.com/csharpya
// Es una estructura de datos, que puede tener 
// cero, uno o muchos elementos DEL MISMO TIPO
// y en principio el numero de elementos es FIJO
public class EjemploArrays
{
	public static void Main()
	{
		// ArrayDeDatosPrimitivos();
		//		ArrayDeFloats();
		//	ArrayDeStrings();
		ArrayEjercicioUno();
		float[] ataques = {3.2f, 1.7f, 2.4f};
	}
	
	/* public static void ArrayDeDatosPrimitivos()
	{
		Console.WriteLine("Array enteros");
		// Declaracion <tipo> [] <nombreVariable>
		int[] numerosPares;
		// Iniciar con el numero de elementos
		// new se encarga de reservar la MEMORIA para lo que sea
		
		numerosPares = new int [4];
		// para asignar un valor en una posicion:
		numerosPares[0] = 2;
		numerosPares[1] = 4;
		numerosPares[2] = 6;
		numerosPares[3] = 8;  // el ultimo elemento esta en la posicion 
							  // longitud de array (-1)
		
		for (int i = 0; i < numerosPares.Length ; i = i + 1) {
			Console.WriteLine("Elemento " + i + " = " + numerosPares[i]);
		
		}
		
	}
	public static void ArrayDeFloats(){
		float[] fuerzas = new float[5];
		for (int i = 0; i<= fuerzas.Length - 1; i += 1){
			fuerzas[i] = 1.2345f + i;
		}
		Console.WriteLine("¿Que fuerza quieres ver?");
		string strTecla = Console.ReadKey().Key.ToString();
		string numero;
		// Para extraer el numero, probamos varias maneras
		// 1 usando string remove quitamos lo que no queremos
		// numero = strTecla.Remove(0, strTecla.Length -1);
		numero = strTecla.Remove(0, strTecla.Length -1);
		// 2 usando el string como si fuera un array unidim de char
		numero = "" + strTecla.Substring(strTecla.Length - 1, 1);
		// 3 usando la tipica para estos casos que es el substring
		
		int pos = Int32.Parse(numero);
		Console.WriteLine("La fuerza num " + pos + " es de: ");
		Console.WriteLine( fuerzas[pos] + " newtons");
	}
	public static void ArrayDeStrings(){
		Console.WriteLine("¿cuantos nombres quiere guardar?");
		string strCantidad = Console.ReadLine();
		int cantidad = Int32.Parse(strCantidad);
		
		
		string[] nombres;
		nombres = new string[cantidad];
		for (int i = 0; i < cantidad; i++) {
			string nuevoNombre = Console.ReadLine();
			nombres[i] = nuevoNombre;
		}
		Console.WriteLine("Listado de nombres: ");
		
		for (int i = 0; i < cantidad; i++) {
			Console.WriteLine($" - {i} = } {nombres[i]} ");
		// Console.WriteLine(" - " + i + " = " + nombres[i] ");
		
		}
		*/
	
	public static void ArrayEjercicioUno(){
		{	
			string resultado;
			resultado = "" ;
		}
		int varA = 3.2f;
		int varB = 1.7f;
		int varC = 2.4f;
		
	
		string varA = "3.2f";
		string varB = "1.7f";
		string varC = "2.4f";
		
		
		
		int aEntero = Int32.Parse( varA );
		int bEntero = Int32.Parse( varB );
		int cEntero = Int32.Parse( varC );
		
		
		bool variableBooleana = true;
		Console.WriteLine("variableBooleana si " + variableBooleana);
		
		if (variableBooleana == true)
		{
		Console.WriteLine(varA + varB + varC);
		resultado = resultado + ( aEntero + bEntero + cEntero );
		Console.WriteLine( resultado );
		}
	}
	// DEBERES generar un array igual (con dos lejos y cuatro cerca [trues]) y mostrarlo
}