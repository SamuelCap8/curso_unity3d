using System;


public class EjemploArrays
{
	public static void Main()
	{
		EjercicioArrayAtaques();
		
	}
	public static void EjercicioArrayAtaques()
	{
		float[] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool[] siCercanos = {false, true, true, false, true, true};
		
		float totalAtaques;
		totalAtaques = CalcAtaqueTotalCercanos( ataques, siCercanos);
		
		bool[] siCercanosJug2 = {true, true, false, false, true, false};
		float totalAtqJug2 = CalcAtaqueTotalCercanos( ataques, siCercanosJug2 );
		
		Console.WriteLine("Total = "+ totalAtaques);
		Console.WriteLine("Total = "+ totalAtqJug2);
	
		// Ejercicio 3
		float totalMay_3 = CalcAtaqueMayoresQueTope(ataques, 3f);
		Console.WriteLine("Total de > 3 = " + totalMay_3);
		Console.WriteLine(" Ataque Maximo = " + CalcAtaqueMaximo(ataques));
	}

	public static float CalcAtaqueTotalCercanos(float[] ataques, bool[] siCercanos)
	{
		float total;
		
		total = 0; // Inicializar variables!!
		// Para cuando los ataques sumen sean los verdaderos
		
		for (int i = 0; i < ataques.Length; i = i + 1)
		{	
			Console.WriteLine("Valor de " + i + " es " + ataques[i]);
			if (siCercanos[i]) {
				total = total + ataques[i];
			}
		}
		return total;
	}
	
	public static float CalcAtaqueMayoresQueTope(float[] ataques, float tope)
	{
		float total = 0;
		
		for (int i = 0; i < ataques.Length; )
		{
			if(ataques[i] > tope) {
				total += total + ataques[i];
			}
			i++;
		}
		return total;		
	}
	 public static float CalcAtaqueMaximo(float[] ataques)
	{
		float valMax = ataques[0];
		
		for (int i = 0; i < ataques.Length; i++)
		{
			if ( ataques[i] > valMax )
			{
				valMax = ataques[i];
			}
		

		
		}
		return valMax;
		
	}

	
}