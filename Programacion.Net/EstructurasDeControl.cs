using System;
 
public class EstructurasDeControl {
	public static void Main () 
	{
		
	/*
	static void EjemploIfSimple()
	{
	static void EjemploIfComplicado()
	{
	static void EjercicioIfSumas()
	{
		
	EjemploBucleWhile();
	
	EjemploBucleFor();
	*/
	
	EjemploBucleDoWhile();
	EjemploIfConsecutivos();	
	 
	}
	static void EjemploIfSimple()
	{
	
	
	// Condicional simple: if [condicion(booleano)] instruccion;
		if (false) Console.WriteLine("Pues va a ser que no");
		if (true) Console.WriteLine("Pues si");
		//bool oSioNo = true;
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues tambien si");
		// O recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 == 5");
		if (4 > 7) Console.WriteLine("Pues esto tampoco se muestra");
	}

		static void EjemploIfComplicado()
		{
		//if complicado o complejo:  
		//if (bool) inastruccionVerdad;else[else significa (si no se cumple lo anterior)] instruccionFalso
		if (4 >= 7) Console.WriteLine("4>=7");
		else Console.WriteLine("4 < 7");
		//Podemos separar en varias lineas
		if ("Hola" != "hola") Console.WriteLine("Son.dist");
		else Console.WriteLine("Son =");	
		
		
	}	
		static void EjercicioIfSumas()
	{
		//Ejercicios
		//string numA = "20", numB = "30", numC = "40";
		//int resultado = 50;
		// Suma las 3 combinaciones (A+B, B+C y A+C) y que el problema diga cual
		//es igual al resultado
		int Finresultado = 50;
		
		string numD = "20", numE = "30", numF = "40";
		
		int dEntero = Int32.Parse( numD );
		int eEntero = Int32.Parse( numE );
		int fEntero = Int32.Parse( numF );
		
		int Dresultado =  + ( dEntero + eEntero );
		int Eresultado =  + ( eEntero + fEntero );
		int Fresultado =  + ( dEntero + fEntero );
		
		
		if (Dresultado != Finresultado) Console.WriteLine("Son.dist"); else Console.WriteLine("Son ="); 
		if (Eresultado != Finresultado) Console.WriteLine("Son.dist"); else Console.WriteLine("Son =");
		if (Fresultado != Finresultado) Console.WriteLine("Son.dist"); else Console.WriteLine("Son =");
		

	} 

	static void EjemploIfConsecutivos ()
	{
		Console.WriteLine("Introduzca una opcion: ");
		Console.WriteLine(" 1 - Opcion primera ");
		Console.WriteLine("2 - Opcion segunda ");
		Console.WriteLine("3 - Opcion tercera ");
		Console.WriteLine(" (*) - cualquier otra opcion ");
		
		
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
		
		Console.WriteLine(" >> " + caracter);
		
		if (caracter == "NumPad1" || caracter == "D1" )
			Console.WriteLine("Has elegido la Primera ");
		else if (caracter == "NumPad2" || caracter == "D2" )
			 Console.WriteLine("Has elegido la Segunda ");
		else if (caracter == "NumPad3" || caracter == "D3" )
			Console.WriteLine("Has elegido la Tercera ");
		else if (caracter == "NumPad4" || caracter == "D4" )
			Console.WriteLine(" Has elegido otra opcian "); 
		else	
			Console.WriteLine(" OPCION NO CONOCIDA ");


	// El bucle while puede crear cualquier tipo de bucle
	// Puede que nunca se ejecute (si el booleano (condicion) es false de entrada)
	// ... o puede que sea un bucle infinito, si la condicion siempre es true
	// o un bucle normal con una duracion determinada

	}
	static void EjemploBucleWhile()
	{
		Console.WriteLine("Antes del Bucle");
		
		while (false)
		{
			Console.WriteLine("Instruccion que se repite");
		}
		int contador = 0;
		
		while (contador < 10)
		{	
			Console.WriteLine("Contador = " + contador);
			contador = contador + 1;
		}
		bool siContinuar = true;
		while (siContinuar)
		{
			Console.WriteLine("Estamos en el bucle. ¿Desea salir?");
			string tecla = Console.ReadKey().Key.ToString();
			if (tecla == "S" || tecla == "s")
			{
				siContinuar = false;
			
			}
			
		}
	
		Console.WriteLine("Fin de bucle");
		
	}
	static void EjemploBucleFor() {
		// Un bucle for es un while que se suele usar con un contador
		// for (<inicializacion>) ; <condicion> ; <incremento> )
		//		instruccion; ó bien un {bloque que se repite}
		for (int contador = 0 ; contador < 10 ; contador = contador + 1)
		{
			Console.WriteLine("contador = " + contador);
		

		}
		
	}
	static void EjemploBucleDoWhile() {
		do{
			Console.WriteLine("Al menos una vez");
		} while (false);
		bool siSalir = true;
		
		do {
			Console.WriteLine("Hasta que la condicion sea falsa");
			siSalir = ! true;
		} while ( ! siSalir);
		
		
	}
}
