using System;

public class TiposDeDatos {
	public static void Main() {
		byte unByte = 255; //Nº entero entre 0 y 255, 8 bits
		char unCaracter = 'A'; // Un caracter tambien es un numero
		int numEntero = 1000; // numero entre -2.000.000.000 y + 2.000.000.000
							  // porque ocupa 4 bytes. 2^32 = 4.000.000.000
		
		
		
		
		Console.WriteLine("Byte: " + unByte + "char: " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + "bytes");
		Console.WriteLine("El entero vale " + numEntero);
		numEntero = 1000000000; // mil millones
		Console.WriteLine("Ahora el entero vale " + numEntero);
		
		
		
		
		// PARA GUARDAR NUMEROS MAS LARGOS
		long enteroLargo = 5000000000L; // 8 bytes
		// para determinar que es double introducimos 
		//una L mayus al final
		Console.WriteLine("Entero largo vale" + enteroLargo);
		
		// tipos de decimales : float (4 bytes) y double (8 bytes)
		// Precision es de : 7-8 cifras. 
		float numDecimal = 1.23456789F; // 4 bytes
		// para determinar que es float introducimos 
		//una f mayus o minus al final
		Console.WriteLine("Num Decimal float vale" + numDecimal);
		// Para mas precision: double
		double numDoblePrecision = 12345.6789123456789;
	    Console.WriteLine("Num decimal doble vale " + numDoblePrecision);
		
		// Para guardar si/no, verdad/mentira , cero/uno
		bool variableBooleana = true;
		Console.WriteLine("variableBooleana vale " + variableBooleana);
		
		// podemos guardar comparaciones, condiciones, etc...
		variableBooleana =  numDecimal > 1000;
		Console.WriteLine("variableBooleana ahora es " + variableBooleana);
		variableBooleana =  numDecimal <= 1000;
		Console.WriteLine("variableBooleana ahora es " + variableBooleana);
		
		
		string cadenaDeTexto = "Pues eso, una cadena de texto";
		Console.WriteLine(cadenaDeTexto);
		Console.WriteLine(cadenaDeTexto+ "que" + "permite concatenacion");
		// lo que no permite son conversiones invalidas:
		//int otroNumero = 5.234f
		
		
		



	}
}