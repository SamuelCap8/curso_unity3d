﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public struct AparicionEnemigo
{
    public int indiceEnem;
    public float posInicioX;
    public float tiempoInicio;
    
    [HideInInspector]
    public bool yaHaAparecido;
}

