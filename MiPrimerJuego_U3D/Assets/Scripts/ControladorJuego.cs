﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    // debo meter los prefabs en un array y comentarlos
    public GameObject[] enemigos;
    public AparicionEnemigo[] aparicionesEnem;
    /*    
    public GameObject prefabEnemigoLata;
    public GameObject enemigo_anilla_1;
    public GameObject enemigo_lata_2;*/
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    private float timeIni;
    private int enemActual;
    //private int puntosAnt; // Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {
        timeIni = Time.time;
        enemActual = 0;
        // this.InstanciarEnemigo();
    }
    // Update is called once per frame
    void Update()
    {
        {
            textoVidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
            textoPuntos.GetComponent<Text>().text = "Puntos: " + this.puntos;
            // Para saber si un enemigo tiene que aparecer tenemos que 
            //saber cuanto tiempo ha pasado desde el inicio del nivel hasta el momento actual 
            //(frame actual) y si es superior al tiempo configurado en la configuracion
            float tiempoActual = Time.time - timeIni;
            if (tiempoActual > aparicionesEnem[enemActual].tiempoInicio)
            {
                // si no ha aparecido 
                if ( ! aparicionesEnem[enemActual].yaHaAparecido) 
                { 
                    this.InstanciarEnemigo();
                    aparicionesEnem[enemActual].yaHaAparecido = true;
                    enemActual++;
                }
                
            }

        }
        {
            if (Input.GetKey(KeyCode.Space) == true)
            {
                this.InstanciarEnemigo();
            }


        }
    
    
    
    }

    // Esto es un nuevo método 
    // (acción, conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        // Estamos asignando un nuevo valor a la puntuación,
        // que es los puntos actuales + 10 ptos.
        this.puntos = this.puntos + 10;     // this.puntos  += 10;
        // this.InstanciarEnemigo();
    }
    public void CuandoPerdemosEnemigo()
    {
        this.vidas = this.vidas - 1;        // this.vidas -= 1;   this.vidas--;
        // Lo he comentado por que duplica para dos jugadores el numero de enemigos
        // 
        

    }
    public void InstanciarEnemigo()
    {
        int numEnemigo = Random.Range(0, 3);
    
        GameObject.Instantiate(enemigos[numEnemigo]);
        
        


    }



}

