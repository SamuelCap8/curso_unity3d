﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuegoDos : MonoBehaviour
{
    // debo meter los prefabs en un array y comentarlos
    public GameObject[] enemigos;
    /*
    public GameObject prefabEnemigoLata;
    public GameObject enemigo_anilla_1;
    public GameObject enemigo_lata_2;*/
    public int vidas_2 = 7;
    public int puntos_2 = 0;
    public GameObject textoVidas_2;
    public GameObject textoPuntos_2; 

    //private int puntosAnt; // Puntos del frame anterior

    // Start is called before the first frame update
    void Start()
    {
        this.InstanciarEnemigo();
    }
    // Update is called once per frame
    void Update()
    {
        {
            textoVidas_2.GetComponent<Text>().text = "Vidas 2: " + this.vidas_2;
            textoPuntos_2.GetComponent<Text>().text = "Puntos 2: " + this.puntos_2;
        }
        {
            /*if (Input.GetKey(KeyCode.Space) == true)
            {
                this.InstanciarEnemigo();
            }*/
        }
    }
    // Esto es un nuevo método 
    // (acción, conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        // Estamos asignando un nuevo valor a la puntuación,
        // que es los puntos actuales + 10 ptos.
        this.puntos_2 = this.puntos_2 + 10;     // this.puntos  += 10;
        this.InstanciarEnemigo();
    }
    public void CuandoPerdemosEnemigo()
    {
        this.vidas_2 = this.vidas_2 - 1;        // this.vidas -= 1;   this.vidas--;
           // Lo he comentado por que duplica para dos jugadores el numero de enemigos
            // this.InstanciarEnemigo();
            


    }
    public void InstanciarEnemigo()
    {
        int numEnemigo = Random.Range(0, 3);

        GameObject.Instantiate(enemigos[numEnemigo]);




    }



}



