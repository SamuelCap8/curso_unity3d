﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Lata2 : MonoBehaviour
{
    public float velocidad; // Por defecto, cero
    GameObject jugador;
    GameObject jugador_2;
    // pruebo una cosa
    


    // Start se llama la primera vez, primer frame
    void Start()
    {
        float posInicioX = Random.Range(-10, 10);
        this.transform.position = new Vector3(posInicioX, 10, 1);
        // Buscamos un GameObject por su nombre, sólo hacer en Start()
        jugador = GameObject.Find("jugador_caballito");
        jugador_2 = GameObject.Find("jugador_caballito_2");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position
            + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);

            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)
            {
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigo();

            }
           // else
            //{
               // GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();
            //}
           else if (this.transform.position.x >= jugador_2.transform.position.x - 3.2f / 2
                           && this.transform.position.x <= jugador_2.transform.position.x + 3.2f / 2)
            {
                GameObject.Find("Controlador_Juego_2").GetComponent<ControladorJuegoDos>().CuandoCapturamosEnemigo();

            }
            else 
            {
                GameObject.Find("Controlador_Juego_2").GetComponent<ControladorJuegoDos>().CuandoPerdemosEnemigo();
                // abajo pruebo una cosa
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();

            }

            Destroy(this.gameObject);

        }

    }


    
}
