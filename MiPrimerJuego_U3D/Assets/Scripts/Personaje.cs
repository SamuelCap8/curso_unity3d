﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    // float es un número decimal
    public float velocidad = 40;    // 40 es valor x defecto

    // Update is called once per frame
    void Update()
    {
        // Cuando se pulsa <--
        if (Input.GetKey(KeyCode.LeftArrow) == true)
        {
            // Creamos el vector de movimiento, a partir del cálculo que
            // influye la velocidad (40), el vector hacia la izquierda (-1, 0, 0):    (-40, 0, 0)
            // Para que se mueva -40 unid. por segundo en vez de por cada frame, 
            // multiplicamos por el incremento del tiempo (aprox. 0.02 seg, 50 FPS)   (0.8, 0, 0)
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
                            //     unid / seg   x   1    x   seg/frame    =   unidades/frame
            // Una vez que se ha calculado, aplicamos el movimiento
            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posición en el eje X es menor que -10 (margen izq)
            if (this.GetComponent<Transform>().position.x < -10 )
            {
                // entonces recolocamos en el margen izq
                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                Debug.Log("Choque a la izquierda: ");
            }
        }
        // Cuando se pulsa -->
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            if (this.GetComponent<Transform>().position.x >   10)
            {
                // entonces recolocamos en el margen izq
                this.GetComponent<Transform>().position = new Vector3( 10, 0, 0);
                Debug.Log("Choque a la izquierda: ");
            }
        }
    }
}
