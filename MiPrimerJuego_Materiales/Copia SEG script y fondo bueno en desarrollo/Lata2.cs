﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lata2 : MonoBehaviour
{
    public float velocidad;
    GameObject jugador;   

    void Start()
    {
        float posInicioX = Random.Range(-17, 20);
        this.transform.position = new Vector3(posInicioX, 13, 1);
        jugador = GameObject.Find("Jugador-Caballito");
    }
    void Update()

    {
        Vector3 movAbajo = velocidad
            * new Vector3(0, -1, 0)
            * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);

            if (this.transform.position.x == jugador.transform.position.x)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
